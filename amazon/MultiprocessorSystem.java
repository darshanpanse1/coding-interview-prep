import java.util.Comparator;

public class MultiprocessorSystem {
    public static int processTime(int num, int[] ability, long processes) {
        int count = 0;
        PriorityQueue<Integer> pq = new PriorityQueue<>(Comparator.reverseOrder());
        for(int abi : ability) {
            pq.offer(abi);
        }
        while(processes > 0) {
            int abi = pq.poll();
            processes -= abi;
            pq.offer(abi/2);
            count++;
        }
        return count;
    }
}