import java.util.*;

/**
 * a.k.a.
 * *************** Smallest Negative Balance ************
 * Note -10 < -2
 */

public class DebtRecord {
  public String[] getDebtRecord(String[] debts, int n) {
    Map<String, Integer> map = new HashMap<>();
    Queue<Map.Entry<String,Integer>> minHeap = new PriorityQueue<>(
      (Map.Entry<String, Integer> e1, Map.Entry<String, Integer> e2) -> {
        if (e1.getValue() == e2.getValue()) {
          return e1.getKey().compareTo(e2.getKey());
        }
        return e1.getValue() - e2.getValue();
      });

    for (String record: debts) {
      String[] recordArr = record.split("\\s+");
      String borrower = recordArr[0];
      String lender = recordArr[1];
      int amount = Integer.parseInt(recordArr[2]);

      map.put(borrower, map.getOrDefault(borrower, 0) - amount);
      map.put(lender, map.getOrDefault(lender, 0) + amount);
    }

    for(Map.Entry<String, Integer> e: map.entrySet()) {
      minHeap.offer(e);
    }

    List<String> result = new ArrayList<>();
    int min = -1;
    while (!minHeap.isEmpty() && minHeap.peek().getValue() <= min) {
      Map.Entry<String, Integer> top = minHeap.poll();
      min = top.getValue();
      result.add(top.getKey());
    }

    if (result.isEmpty()) {
      return new String[]{"Nobody has a negative balance"};
    }

    return result.toArray(new String[0]);
  }

  public static void main(String[] args) {
    DebtRecord dr = new DebtRecord();
    test1(dr);
  }

  private static void test1(DebtRecord dr) {
    String[] debts = {"Alex Blake 2", "Blake Alex 2", "Casey Alex 5",
     "Blake Casey 7", "Alex Blake 4", "Alex Casey 4"};
    int n = 6;

    String[] output = dr.getDebtRecord(debts, n);

    for (String record: output) {
      System.out.print(record + " ");
    }
  }
}