/**
 * Given an array containing only positive integers, return if you can pick 
 * two integers from the array which cuts the array into three pieces such that 
 * the sum of elements in all pieces is equal.

Example 1:
Input: array = [2, 4, 5, 3, 3, 9, 2, 2, 2]
Output: true
Explanation: choosing the number 5 and 9 results in three pieces [2, 4], [3, 3] and [2, 2, 2]. Sum = 6.

Example 2:
Input: array =[1, 1, 1, 1],
Output: false
 */

public class LoadBalancer {
  public boolean loadBalance(int[] arr) {
    int i = 1, j = arr.length - 2;
    int sum1 = arr[i];
    int sum2 = findSum2(arr);
    int sum3 = arr[arr.length - 1];

    while (i < j - 1) {
      if (sum1 < sum3) {
        sum1 += arr[i];
        i++;
        sum2 -= arr[i];
      }

      if (sum3 < sum1) {
        sum3 += arr[j];
        j--;
        sum2 -= arr[j];
      }

      if (sum2 < sum1 || sum2 < sum3) {
        return false;
      }

      if (sum1 == sum2 && sum2 == sum3) {
        return true;
      }
    }

    return true;
  }

  private int findSum2(int[] arr) {
    int sum = 0;
    for (int i = 2; i < arr.length - 2; i++) {
      sum += arr[i];
    }

    return sum;
  }
}