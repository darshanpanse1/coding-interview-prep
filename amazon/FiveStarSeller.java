import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * a.k.a. 
 * ****************** Maximum Average Pass Ratio ********************
 * 
 * Given: 2D integer array classes where classes[i] = [pass, total] and integer
 * extraStudents who are guaranteed to pass. Distribute the extraStudents in the
 * classes to maximize the avg pass ratio. 
 * Example 1: 
 * Input: classes = [[1,2],[3,5],[2,2]] and extraStudents = 2 
 * Output: 0.78333 
 * Explanation: Assign the 2 extraStudents to the first class. 
 * Avg pass ratio becomes (3/4 + 3/5 + 2/2) / 3 = 0.78333
 */

public class FiveStarSeller {
  private class Classes {
    int pass;
    int total;
    double ratioIncreament;

    Classes(int pass, int total, double ratioIncreament) {
      this.pass = pass;
      this.total = total;
      this.ratioIncreament = ratioIncreament;
    }

    int getPass() {
      return pass;
    }

    int getTotal() {
      return total;
    }

    double getRatioIncreament() {
      return ratioIncreament;
    }
  }

  public double maximizeRatio(int[][] classes, int extraStudents) {
    Queue<Classes> maxheap = new PriorityQueue<>(classes.length,
        Comparator.comparingDouble(Classes::getRatioIncreament).reversed());

    double ratioSum = 0;
    for (int[] c : classes) {
      int pass = c[0];
      int total = c[1];
      double ratioIncreament = (pass + 1) / (double) (total + 1) - pass / (double) total;
      ratioSum += pass / (double) total;
      maxheap.offer(new Classes(pass, total, ratioIncreament));
    }
    for (int i = 0; i < extraStudents; i++) {
      Classes top = maxheap.poll();
      ratioSum += top.ratioIncreament;
      top.pass++;
      top.total++;
      top.ratioIncreament = (top.pass + 1) / (double) (top.total + 1) - top.pass / (double) top.total;
      maxheap.offer(top);
    }

    return ratioSum / classes.length;
  }

  public static void main(String[] args) {
    FiveStarSeller fss = new FiveStarSeller();
    test1(fss);
    test2(fss);
  }

  private static void test1(FiveStarSeller fss) {
    int[][] classes = {{1,2}, {3,5}, {2,2}};
    int extraStudents = 2;
    System.out.println(fss.maximizeRatio(classes, extraStudents));
  }
  
  private static void test2(FiveStarSeller fss) {
    int[][] classes = {{2,4}, {3,9}, {4,5}, {2,10}};
    int extraStudents = 4;
    System.out.println(fss.maximizeRatio(classes, extraStudents));
  }
}