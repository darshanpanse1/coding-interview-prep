/**
 * Given: 
 * 
 * int time[n]: an array of n integers where the value at index i is the time in seconds when the ith person will come.
 * int direction[n]: an array of n integers where the value at index i is the direction of the ith person.
 * direction 0 = enter and 1 = exit.
 * 
 * Return:
 * int result[n]: an array of integers where the value at inde i is the time when the ith person will pass the turnstile.
 * 
 * If two people want to enter and exit resp. at the exact same time then:
 * 
 * 1. If in the previous second the turnstile was not used then exit first.
 * 2. If in the previous second the turnstile was used for exit then exit.
 * 3. If in the previous second the turnstile was used to enter then enter.
 */

 public class Turnstile {
   public int[] getTimes(int[] time, int[] direction) {
    int[] result = new int[time.length];

    return result;
   }
 }