import java.util.HashMap;
import java.util.Map;

/**
 * Design a data structure that follows the constraints of a Least Recently Used
 * (LRU) cache.
 * 
 * Implement the LRUCache class:
 * 
 * LRUCache(int capacity) Initialize the LRU cache with positive size capacity.
 * int get(int key) Return the value of the key if the key exists, otherwise
 * return -1. void put(int key, int value) Update the value of the key if the
 * key exists. Otherwise, add the key-value pair to the cache. If the number of
 * keys exceeds the capacity from this operation, evict the least recently used
 * key. The functions get and put must each run in O(1) average time complexity.
 * 
 * 
 * 
 * Example 1:
 * 
 * Input ["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get",
 * "get"] [[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]] Output
 * [null, null, null, 1, null, -1, null, -1, 3, 4]
 * 
 * Explanation LRUCache lRUCache = new LRUCache(2); lRUCache.put(1, 1); // cache
 * is {1=1} lRUCache.put(2, 2); // cache is {1=1, 2=2} lRUCache.get(1); //
 * return 1 lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1,
 * 3=3} lRUCache.get(2); // returns -1 (not found) lRUCache.put(4, 4); // LRU
 * key was 1, evicts key 1, cache is {4=4, 3=3} lRUCache.get(1); // return -1
 * (not found) lRUCache.get(3); // return 3 lRUCache.get(4); // return 4
 * 
 * 
 * Constraints:
 * 
 * 1 <= capacity <= 3000 0 <= key <= 104 0 <= value <= 105 At most 2 * 105 calls
 * will be made to get and put.
 */

class LRUCache {
  private class Node {
    int key;
    int val;
    Node next;
    Node prev;

    Node(int key, int val) {
      this.key = key;
      this.val = val;
    }
  }

  private Map<Integer, Node> map;
  private Node head;
  private Node tail;
  private int currSize;
  private int capacity;

  public LRUCache(int capacity) {
    this.map = new HashMap<>();
    this.currSize = 0;
    this.capacity = capacity;
  }

  public int get(int key) {
    int val = -1;
    Node node = this.map.get(key);
    if (node != null) {
      makeHead(node);
      val = node.val;
    }

    return val;
  }

  public void put(int key, int value) {
    Node node = this.map.get(key);
    if (node != null) {
      node.val = value;
      makeHead(node);
    } else {
      node = new Node(key, value);
      if (this.currSize < capacity) {
        makeHead(node);
        this.currSize++;
      } else {
        deleteTail();
        this.currSize--;
        makeHead(node);
        this.currSize++;
      }
      this.map.put(key, node);
    }
  }

  private void makeHead(Node node) {
    if (this.currSize == 0) {
      this.head = node;
      this.tail = node;
    } else if (this.head != node) {
      if (node.next != null && node.prev != null) {
        Node p = node.prev;
        Node n = node.next;
        p.next = n;
        n.prev = p;
      }

      if (node.next == null && node.prev != null) {
        Node p = node.prev;
        p.next = null;
        this.tail = p;
      }

      node.next = this.head;
      this.head.prev = node;
      this.head = node;
    }
  }

  private void deleteTail() {
    this.map.remove(this.tail.key);
    if (this.capacity == 1) {
      this.head = null;
      this.tail = null;
    } else {
      this.tail = this.tail.prev;
      this.tail.next = null;
    }
  }
}

/**
 * Your LRUCache object will be instantiated and called as such: LRUCache obj =
 * new LRUCache(capacity); int param_1 = obj.get(key); obj.put(key,value);
 */