public class LargestRectangleHistogram {
    public int largestRectangleArea(int[] heights) {
        // 1. find the subarray for each element where the element is min.
        int[][] subarrays = findSubArrays(heights);
        
        // 2. find the max area of the rectangle for each subarray.
        // 3. find the max of all the areas.
        int maxArea = 0;
        for (int i=0; i<heights.length; i++) {
          int area = calculateArea(heights[i], subarrays[i][0], subarrays[i][1]);
          maxArea = Math.max(area, maxArea);
        }
        
        return maxArea;
      }
      
      private int[][] findSubArrays(int[] heights) {
        Deque<Integer> stack = new ArrayDeque<>();
        int[][] subarrays = new int[heights.length][2];
        
        for (int i=0; i<heights.length; i++) {
          if (stack.isEmpty()) {
            stack.addFirst(i);
            subarrays[i][0] = 0;
          } else {
            if (heights[stack.peekFirst()] < heights[i]) {
              subarrays[i][0] = stack.peekFirst() + 1;
              stack.addFirst(i);
            } else {
              while (!stack.isEmpty() && heights[stack.peekFirst()] >= heights[i]) {
                stack.removeFirst();
              }
              if (stack.isEmpty()) {
                stack.addFirst(i);
                subarrays[i][0] = 0;
              } else {
                subarrays[i][0] = stack.peekFirst() + 1;
                stack.addFirst(i);
              }
            }
          }
        }
        
        while (!stack.isEmpty()) {
          stack.removeFirst();
        }
        
        for (int i=heights.length-1; i>=0; i--) {
          if (stack.isEmpty()) {
            stack.addFirst(i);
            subarrays[i][1] = heights.length-1;
          } else {
            if (heights[stack.peekFirst()] < heights[i]) {
              subarrays[i][1] = stack.peekFirst() - 1;
              stack.addFirst(i);
            } else {
              while (!stack.isEmpty() && heights[stack.peekFirst()] >= heights[i]) {
                stack.removeFirst();
              }
              if (stack.isEmpty()) {
                stack.addFirst(i);
                subarrays[i][1] = heights.length-1;
              } else {
                subarrays[i][1] = stack.peekFirst() - 1;
                stack.addFirst(i);
              }
            }
          }
        }
        
        return subarrays;
      }
      
      private int calculateArea(int height, int left, int right) {
        return (right - left + 1) * height;
      }
}