/**
 * No. of requests cannot exceed 3 in a given second.
 * No. of requests cannot exceed 20 in any 10 second window calculated as max(1, T-9).
 * No.of requests cannot exceed 60 in any 1 minute window calculated as max(1, T-59).
 * 
 * Objective: Count the no. of dropped requests.  
 * 
 * Example1:
 * n = 27
 * requestTimes = [1,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,7,11,11,11,11]
 */
public class ThrottlingGateway {
  public int countDropped(int[] requestTimes, int n) {
    int dropCount = 0;
    int[] startIndexs = {0, 0, 0};
    int[] winStarts = {0, 0, 0};
    int[] winSizes = {0, 0, 0};
    int[][] rules = {{0,3}, {9,20}, {59, 60}};

    for (int i=0; i<n; i++) {
      boolean dropped = false;
      for (int j=0; j<rules.length; j++) {
        winSizes[j]++;
        winStarts[j] = Math.max(1, requestTimes[i] - rules[j][0]);
        while (requestTimes[startIndexs[j]] < winStarts[j]) {
          startIndexs[j]++;
          winSizes[j]--;
        }
        if (!dropped && winSizes[j] > rules[j][1]) {
          dropped = true;
          dropCount++;
        }
      }
    }

    return dropCount;
  }

  public static void main(String[] args) {
    ThrottlingGateway tg = new ThrottlingGateway();
    test1(tg);
    test2(tg);
    test3(tg);
  }

  private static void test1(ThrottlingGateway tg) {
    int n = 27;
    int[] requestTimes = {1,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,7,11,11,11,11};
    System.out.println("Drop Count: " + tg.countDropped(requestTimes, n));
  }

  private static void test2(ThrottlingGateway tg) {
    int n = 5;
    int[] requestTimes = {1,1,1,1,2};
    System.out.println("Drop Count: " + tg.countDropped(requestTimes, n));
  }
  private static void test3(ThrottlingGateway tg) {
    int n = 21;
    int[] requestTimes = {1,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7};
    System.out.println("Drop Count: " + tg.countDropped(requestTimes, n));
  }
}
