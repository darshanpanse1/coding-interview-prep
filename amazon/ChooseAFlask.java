import java.util.*;
/**
 * Given:
 * 
 * numOrders = integer representing number of orders. 
 * requirements = array of ntegers where each element represents the volume of each order 
 * flaskTypes = integer representing number of flask types 
 * totalMarks = integer representing number of markings 
 * markings = array of pairs of integers where each pair represents (index of flask, marking)
 * 
 * Output: Flast index with least wastage else -1 if no flask fullfills requirements
 * 
 * wastage = marking - volume
 */

public class ChooseAFlask {
  public int chooseAFlask(int numOrders, int[] requirements, int flaskTypes, int totalMarks, int[][] markings) {
    Arrays.sort(requirements);
    
    int minWaste = Integer.MAX_VALUE;
    int flaskIndex = -1;
    int i = 0, j = 0;
    int flaskWaste = 0;
    boolean checkResetCondition = false;
    while (j < markings.length) {
      if (i == requirements.length) {
        if (flaskWaste < minWaste) {
          minWaste = flaskWaste;
          flaskIndex = markings[j][0];
        }
        while (j < markings.length && markings[j][0] == markings[j-1][0]) {
          j++;
        }
        i = 0;
        flaskWaste = 0;
        checkResetCondition = false;
      } else {
        if (checkResetCondition && markings[j][0] != markings[j-1][0]) {
          i = 0;
          flaskWaste = 0;
          checkResetCondition = false;
        }
        if (markings[j][1] < requirements[i]) {
          j++;
          checkResetCondition = true;
        } else {
          flaskWaste += markings[j][1] - requirements[i];
          i++;
        }
      }
    }

    return flaskIndex;
  }

  public static void main(String[] args) {
    ChooseAFlask cf = new ChooseAFlask();
    test1(cf);
    test2(cf);
  }

  private static void test1(ChooseAFlask cf) {
    int numOrders = 4;
    int[] requirements = {4,6,6,7};
    int flaskTypes = 3;
    int totalMarks = 9;
    int[][] markings = {{0,3},{0,5},{0,7},{1,6},{1,8},{1,9},{2,3},{2,5},{2,6}};

    System.out.println(cf.chooseAFlask(numOrders, requirements, flaskTypes, totalMarks, markings));
  }

  private static void test2(ChooseAFlask cf) {
    int numOrders = 4;
    int[] requirements = {4,6,6,7};
    int flaskTypes = 1;
    int totalMarks = 1;
    int[][] markings = {{0,3}};

    System.out.println(cf.chooseAFlask(numOrders, requirements, flaskTypes, totalMarks, markings));
  }
}