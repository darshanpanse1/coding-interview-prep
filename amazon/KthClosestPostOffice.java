import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Given an array of points where points[i] = [xi, yi] represents a point on the
 * X-Y plane and an integer k, return the k closest points to the origin (0, 0).
 * 
 * The distance between two points on the X-Y plane is the Euclidean distance
 * (i.e., √(x1 - x2)2 + (y1 - y2)2).
 * 
 * You may return the answer in any order. The answer is guaranteed to be unique
 * (except for the order that it is in).
 * 
 * Example 1: Input: points = [[1,3],[-2,2]], k = 1 Output: [[-2,2]]
 * Explanation: The distance between (1, 3) and the origin is sqrt(10). The
 * distance between (-2, 2) and the origin is sqrt(8). Since sqrt(8) < sqrt(10),
 * (-2, 2) is closer to the origin. We only want the closest k = 1 points from
 * the origin, so the answer is just [[-2,2]].
 * 
 * Example 2: Input: points = [[3,3],[5,-1],[-2,4]], k = 2 Output:
 * [[3,3],[-2,4]] Explanation: The answer [[-2,4],[3,3]] would also be accepted.
 * 
 * 
 * Constraints:
 * 
 * 1 <= k <= points.length <= 104 -104 < xi, yi < 104
 */

public class KthClosestPostOffice {
  public int[][] kClosest(int[][] points, int k) {
    Comparator<List<Integer>> euclideanDistComparator = (List<Integer> p1, List<Integer> p2) -> {
      if (euclideanDist(p1) < euclideanDist(p2))
        return -1;
      if (euclideanDist(p1) > euclideanDist(p2))
        return 1;
      return 0;
    };
    Queue<List<Integer>> minHeap = new PriorityQueue<>(k, euclideanDistComparator);

    for (int i = 0; i < points.length; i++) {
      int[] point = points[i];
      List<Integer> pointList = List.of(point[0], point[1]);
      minHeap.add(pointList);
    }

    int[][] result = new int[k][2];
    for (int i = 0; i < k; i++) {
      List<Integer> point = minHeap.poll();
      int[] pointArr = { point.get(0), point.get(1) };
      result[i] = pointArr;
    }

    return result;
  }

  private double euclideanDist(List<Integer> point) {
    return Math.sqrt(Math.pow(point.get(0).doubleValue(), 2.0) + Math.pow(point.get(1).doubleValue(), 2.0));
  }
}