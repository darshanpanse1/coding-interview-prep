/**
 * Given:
 * A string keysPressed of length n, where keysPressed[i] was the ith key pressed.
 * A sorted array releaseTimes where releaseTimes[i] was the time the ith key was
 * released. 
 * 
 * Find the longest duration.
 * 
 * duration of the ith keypress = releaseTimes[i] - releaseTimes[i-1]
 * duration of 0th keypress = releaseTimes[0]
 * 
 * In case of a tie b/w duration times, return the lexicographically largest key.
 * 
 * Example 1:
 * 
 * Input: releaseTimes = [9,29,49,50], keysPresses = "cbcd"
 * Output: 'c'
 * 
 * Example 2:
 * Input: releaseTimes = [9,29,49,50], keysPresses = "ccbd"
 * Output: 'c'
 */

 public class SlowestKey {
    public char slowestKey(int[] releaseTimes, String keyPresses) {
      int maxDuration = 0;
      int resultIndex = 0;
      for (int i = 0; i < keyPresses.length(); i++) {
        int duration = 0;
        if (i == 0) {
          duration = releaseTimes[i];
        } else {
          duration = releaseTimes[i] - releaseTimes[i-1];
        }
        if (duration > maxDuration) {
          maxDuration = duration;
          resultIndex = i;
        }
        if (duration == maxDuration) {
          if (keyPresses.charAt(i) > keyPresses.charAt(resultIndex)) {
            resultIndex = i;
          }
        }
      }

      return keyPresses.charAt(resultIndex);
    }

    public static void main(String[] args) {
      SlowestKey sk = new SlowestKey();
      test1(sk);
      test2(sk);
    }

    private static void test1(SlowestKey sk) {
      int[] releaseTimes = {9,29,49,50};
      String keyPresses = "cbcd";

      System.out.println(sk.slowestKey(releaseTimes, keyPresses));
    }

    private static void test2(SlowestKey sk) {
      int[] releaseTimes = {9,29,49,50};
      String keyPresses = "ccbd";

      System.out.println(sk.slowestKey(releaseTimes, keyPresses));
    }
 }