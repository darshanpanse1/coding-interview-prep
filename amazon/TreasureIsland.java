public class TreasureIsland {
  private int minDist = Integer.MAX_VALUE;

  /**
   * You have a map that marks the location of a treasure island. Some of the map
   * area has jagged rocks and dangerous reefs. Other areas are safe to sail in.
   * There are other explorers trying to find the treasure. So you must figure out
   * a shortest route to the treasure island. Assume the map area is a two
   * dimensional grid, represented by a matrix of characters. You must start from
   * the top-left corner of the map and can move one block up, down, left or right
   * at a time. The treasure island is marked as X in a block of the matrix. X
   * will not be at the top-left corner. Any block with dangerous rocks or reefs
   * will be marked as D. You must not enter dangerous blocks. You cannot leave
   * the map area. Other areas O are safe to sail in. The top-left corner is
   * always safe. Output the minimum number of steps to get to the treasure.
   * 
   * Example:
   * 
   * Input: [['O', 'O', 'O', 'O'], ['D', 'O', 'D', 'O'], ['O', 'O', 'O', 'O'],
   * ['X', 'D', 'D', 'O']]
   * 
   * Output: 5 Explanation: Route is (0, 0), (0, 1), (1, 1), (2, 1), (2, 0), (3,
   * 0) The minimum route takes 5 steps.
   */
  public int shortestDistanceToTreasure(char[][] graph) {
    shortestDistanceToTreasure(0, 0, graph, new boolean[graph.length][graph[0].length], 0);

    return minDist;
  }

  /**
   * You have a map that marks the locations of treasure islands. Some of the map
   * area has jagged rocks and dangerous reefs. Other areas are safe to sail in.
   * There are other explorers trying to find the treasure. So you must figure out
   * a shortest route to one of the treasure island. Assume the map area is a two
   * dimensional grid, represented by a matrix of characters. You must start from
   * one of the starting point(marked as 'S') of the map and can move one block
   * up, down, left or right at a time. The treasure island is marked as ‘X’ in a
   * block of the matrix. Any block with dangerous rocks or reefs will be marked
   * as ‘D’. You must not enter dangerous blocks. You cannot leave the map area.
   * Other areas ‘O’ are safe to sail in. Output the minimum number of steps to
   * get to any of the treasure. e.g. Input [ [‘S’, ‘O’, ‘O’, 'S', ‘S’], [‘D’,
   * ‘O’, ‘D’, ‘O’, ‘D’], [‘O’, ‘O’, ‘O’, ‘O’, ‘X’], [‘X’, ‘D’, ‘D’, ‘O’, ‘O’],
   * [‘X', ‘D’, ‘D’, ‘D’, ‘O’], ]
   * 
   * Output 3 Explanation You can start from (0,0), (0, 3) or (0, 4). The treasure
   * locations are (2, 4) (3, 0) and (4, 0). Here the shortest route is (0, 3),
   * (1, 3), (2, 3), (2, 4).
   */
  public int shortestDistanceToTreasureII(char[][] graph) {
    for (int i = 0; i < graph.length; i++) {
      for (int j = 0; j < graph[0].length; j++) {
        if (graph[i][j] == 'S') {
          shortestDistanceToTreasure(i, j, graph, new boolean[graph.length][graph[0].length], 0);
        }
      }
    }

    return minDist;
  }

  public void shortestDistanceToTreasure(int i, int j, char[][] graph, boolean[][] visited, Integer stepCount) {
    if (isWithinBounds(i, j, graph, visited, stepCount)) {
      visited[i][j] = true;
      if (graph[i][j] == 'X') {
        minDist = stepCount;
      } else if (graph[i][j] == 'O' || graph[i][j] == 'S') {
        shortestDistanceToTreasure(i, j + 1, graph, visited, stepCount + 1);
        shortestDistanceToTreasure(i + 1, j, graph, visited, stepCount + 1);
        shortestDistanceToTreasure(i, j - 1, graph, visited, stepCount + 1);
        shortestDistanceToTreasure(i - 1, j, graph, visited, stepCount + 1);
      }
      visited[i][j] = false;
    }
  }

  private boolean isWithinBounds(int i, int j, char[][] graph, boolean[][] visited, int stepCount) {
    int iMin = 0;
    int jMin = 0;
    int iMax = graph.length;
    int jMax = graph[0].length;
    return i >= iMin && i < iMax && j >= jMin && j < jMax && graph[i][j] != 'D' && !visited[i][j]
        && stepCount < minDist;
  }

  public static void main(String[] args) {
    TreasureIsland ts = new TreasureIsland();
    // Do not enable both tests at the same time. It leaves minDist in an
    // inconsistent state.
    // test1(ts);
    test2(ts);
  }

  private static void test1(TreasureIsland ts) {
    char[][] graph = { { 'O', 'O', 'O', 'O' }, { 'D', 'O', 'O', 'O' }, { 'X', 'O', 'O', 'O' }, { 'O', 'D', 'D', 'O' } };
    System.out.println(ts.shortestDistanceToTreasure(graph));
  }

  private static void test2(TreasureIsland ts) {
    char[][] graph = { { 'S', 'O', 'S', 'S' }, { 'D', 'O', 'O', 'O' }, { 'X', 'O', 'O', 'X' }, { 'O', 'D', 'D', 'O' } };
    System.out.println(ts.shortestDistanceToTreasureII(graph));
  }
}