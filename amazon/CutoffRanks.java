import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Parameters: scores : List of int cutOffRank : int num: int (denoting amount
 * of scores)
 * 
 * You are given a list of integers representing scores of players in a video
 * game. Players can 'level-up' if by the end of the game they have a rank that
 * is at least the cutOffRank. A player's rank is solely determined by their
 * score relative to the other players' scores. For example:
 * 
 * Score : 10 | Rank 1 Score : 5 | Rank 2 Score : 3 | Rank 3 etc.
 * 
 * If multiple players happen to have the same score, then they will all receive
 * the same rank. However, the next player with a score lower than theirs will
 * receive a rank that is offset by this. For example:
 * 
 * Score: 10 | Rank 1 Score: 10 | Rank 1 Score: 10 | Rank 1 Score : 5 | Rank 4
 * 
 * Finally, any player with a score of 0 is automatically ineligible for
 * leveling-up, regardless of their rank.
 */

public class CutoffRanks {
  public int count(int[] scores, int cutoffRank, int num) {
    // List<Integer> scoreList = Arrays.stream(scores).boxed().collect(Collectors.toList()); // less efficient

    // More efficient. Done in single pass.
    List<Integer> scoreList = new ArrayList<>();
    for (int score: scores) {
      scoreList.add(score);
    }

    scoreList.sort(Comparator.reverseOrder());
    
    int count = 0;
    int rank = 0;
    for (int i=0; i<num; i++) {
      if (scoreList.get(i) == 0) {
        return 0;
      }
      if (i == 0) {
        rank = 1;
        count++;
      } else {
        if (scoreList.get(i) == scoreList.get(i-1)) {
          count++;
        } else {
          rank = i+1;
          if (rank <= cutoffRank) {
            count++;
          }
        }
      }
    }

    return count;
  }

  /**
   * Alternative optimized solution if scores are guaranteed to be int the range [0, 100].
   * @param cutOffRank
   * @param num
   * @param scores
   * @return
   */
  public int cutOffRank(int cutOffRank, int num, int[] scores) {
    if(cutOffRank == 0) return 0;
    int[] cache = new int[101];
    for (int n : scores){
        cache[n]++;
    }
    int  res = 0;
    for (int i = 100; i > 0; i--){
        if (cutOffRank <= 0) break;
        cutOffRank -= cache[i];
        res += cache[i];
    }    
    return res;
}

  public static void main(String[] args) {
    CutoffRanks cr = new CutoffRanks();
    test1(cr);
    test2(cr);
  }

  private static void test1(CutoffRanks cr) {
    int[] scores = {10, 10, 10, 5, 2};
    int cutoffRank = 4;
    int num = 5;
    System.out.println(cr.count(scores, cutoffRank, num));
  }

  private static void test2(CutoffRanks cr) {
    int[] scores = {10, 10, 10, 5, 5, 5, 4, 2};
    int cutoffRank = 7;
    int num = 8;
    System.out.println(cr.count(scores, cutoffRank, num));
  }
}
