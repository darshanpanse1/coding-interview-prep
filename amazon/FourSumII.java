import java.util.*;

/**
 * Given four integer arrays nums1, nums2, nums3, and nums4 and an integer target,
 * return the number of tuples (i, j, k, l) such that:
 * 
 * 0 <= i, j, k, l < n nums1[i] + nums2[j] + nums3[k] + nums4[l] == target
 * 
 * 
 * Example 1:
 * 
 * Input: nums1 = [1,2], nums2 = [-2,-1], nums3 = [-1,2], nums4 = [0,2], target = 0 
 * Output:
 * 2 Explanation: The two tuples are: 1. (0, 0, 0, 1) -> nums1[0] + nums2[0] +
 * nums3[0] + nums4[1] = 1 + (-2) + (-1) + 2 = 0 2. (1, 1, 0, 0) -> nums1[1] +
 * nums2[1] + nums3[0] + nums4[0] = 2 + (-1) + (-1) + 0 = 0 
 * 
 * Example 2:
 * 
 * Input: nums1 = [0], nums2 = [0], nums3 = [0], nums4 = [0], target = 0;
 * Output: 1
 * 
 * 
 * Constraints:
 * 
 * n == nums1.length n == nums2.length n == nums3.length n == nums4.length 1 <=
 * n <= 200 -228 <= nums1[i], nums2[i], nums3[i], nums4[i] <= 228
 */

class FourSumII {
  public int fourSumCount(int[] nums1, int[] nums2, int[] nums3, int[] nums4, int target) {
    Map<Integer, Integer> map = new HashMap<>();

    for (int num3: nums3) {
      for (int num4: nums4) {
        int sum = num3 + num4;
        map.put(sum, map.getOrDefault(sum, 0) + 1);    
      }
    }
    
    int count = 0;
    
    for (int num1: nums1) {
      for (int num2: nums2) {
        int rem = target - (num1 + num2);
        if (map.get(rem) != null) {
          count += map.get(rem);      
        }
      }
    }

    return count;
  }

  public static void main(String[] args) {
    FourSumII fSum = new FourSumII();
    test1(fSum);
  }

  private static void test1(FourSumII fSum) {
    int[] nums1 = {1,1};
    int[] nums2 = {1,1};
    int[] nums3 = {-1,-1};
    int[] nums4 = {-1,-1};
    int target = 0;

    System.out.println(fSum.fourSumCount(nums1, nums2, nums3, nums4, target));
  }
}