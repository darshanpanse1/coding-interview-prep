import java.util.HashSet;
import java.util.Set;

/**
 * Given a string s, find the length of the longest substring without repeating
 * characters.
 * 
 * 
 * 
 * Example 1:
 * 
 * Input: s = "abcabcbb" Output: 3 Explanation: The answer is "abc", with the
 * length of 3. Example 2:
 * 
 * Input: s = "bbbbb" Output: 1 Explanation: The answer is "b", with the length
 * of 1. Example 3:
 * 
 * Input: s = "pwwkew" Output: 3 Explanation: The answer is "wke", with the
 * length of 3. Notice that the answer must be a substring, "pwke" is a
 * subsequence and not a substring. Example 4:
 * 
 * Input: s = "" Output: 0
 * 
 * 
 * Constraints:
 * 
 * 0 <= s.length <= 5 * 104 s consists of English letters, digits, symbols and
 * spaces.
 */

public class SlidingWindow {
  public int lengthOfLongestSubstring(String s) {
    int i = 0, j = 0, localMax = 0, globalMax = 0;
    Set<Character> visited = new HashSet<>();

    while (i < s.length() - globalMax) {
      if (visited.contains(s.charAt(j))) {
        visited.remove(s.charAt(i));
        i++;
        localMax--;
      } else {
        visited.add(s.charAt(j));
        localMax++;
        j++;
      }

      globalMax = Math.max(localMax, globalMax);
    }

    return globalMax;
  }
}