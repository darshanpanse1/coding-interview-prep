import java.util.*;

/**
 * A Company parses logs of online store user transactions/activity to flag
 * fraudulent activity.
 * 
 * The log file is represented as an Array of arrays. The arrays consist of the
 * following data:
 * 
 * [ <# of transactions>]
 * 
 * For example:
 * 
 * [345366 89921 45]
 * 
 * Note: the data is space delimited
 * 
 * So, the log data would look like:
 * 
 * [ [345366 89921 45], [029323 38239 23] ... ] Write a function to parse the
 * log data to find distinct users that meet or cross a certain threshold.
 * 
 * The function will take in 2 inputs: logData: Log data in form an array of
 * arrays
 * 
 * threshold: threshold as an integer
 * 
 * Output: It should be an array of userids that are sorted.
 * 
 * If same userid appears in the transaction as userid1 and userid2, it should
 * count as one occurrence, not two.
 * 
 * Example: Input: logData:
 * 
 * [ [345366 89921 45], [029323 38239 23], [38239 345366 15], [029323 38239 77],
 * [345366 38239 23], [029323 345366 13], [38239 38239 23] ... ] threshold: 3
 * 
 * Output: [029323, 38239, 345366] Explanation: Given the following counts of
 * userids, there are only 3 userids that meet or exceed the threshold of 3.
 * 
 * 345366 -4 , 38239 -5, 029323-3, 89921-1
 * 
 * Note: The input contains a list of strings. Each string is of the format
 * "userid1 userid2 #no of transactions". If the same userid appears in the
 * transaction as userid1 and userid2 means, take this input string [38239 38239
 * 23]. Here the userid1 and userid2 are same ie 38239. So count it as 1 instead
 * of 2. When you print the contents of the map then you will see that 38239 is
 * printed 5 instead of 6. Hope I clarified your question.
 */
public class TransactionLogs {

  static List<String> processLogs(List<String> logs, int threshold) {
    Map<String, Integer> map = new HashMap<>();
    for (String logLine : logs) {
      String[] log = logLine.split(" ");
      map.put(log[0], map.getOrDefault(log[0], 0) + 1);
      if (log[0].equals(log[1])) {
        map.put(log[1], map.getOrDefault(log[1], 0) + 1);
      }
    }

    List<String> userIds = new ArrayList<>();
    for (Map.Entry<String, Integer> entry : map.entrySet()) {
      if (entry.getValue() >= threshold) {
        userIds.add(entry.getKey());
      }
    }

    Collections.sort(userIds, (String s1, String s2) -> Integer.parseInt(s1) - Integer.parseInt(s2));

    return userIds;
  }

  public static void main(String[] args) {
    List<String> input = new ArrayList<>() {
      {
        add("345366 89921 45");
        add("029323 38239 23");
        add("38239 345366 15");
        add("029323 38239 77");
        add("345366 38239 23");
        add("029323 345366 13");
        add("38239 38239 23");
      }
    };

    processLogs(input, 2).forEach(System.out::println);
  }
}