/**
 * There are n cities. Some of them are connected, while some are not. If city a
 * is connected directly with city b, and city b is connected directly with city
 * c, then city a is connected indirectly with city c.
 * 
 * A province is a group of directly or indirectly connected cities and no other
 * cities outside of the group.
 * 
 * You are given an n x n matrix isConnected where isConnected[i][j] = 1 if the
 * ith city and the jth city are directly connected, and isConnected[i][j] = 0
 * otherwise.
 * 
 * Return the total number of provinces.
 * 
 * Example 1: Input: isConnected = [[1,1,0],[1,1,0],[0,0,1]] Output: 2
 * 
 * Example 2: Input: isConnected = [[1,0,0],[0,1,0],[0,0,1]] Output: 3
 */

public class CountProvinces {
  public int findCircleNum(int[][] isConnected) {
    int[] provinces = new int[isConnected.length];
    int numProvinces = 0;
    Deque<Integer> stack = new ArrayDeque<>();

    for (int i = 0; i < isConnected.length; i++) {
      if (provinces[i] == 0) {
        provinces[i] = ++numProvinces;
        stack.addFirst(i);
      }

      while (!stack.isEmpty()) {
        int k = stack.removeFirst();
        for (int j = 0; j < isConnected.length; j++) {
          if (k != j && isConnected[k][j] == 1) {
            if (provinces[j] == 0) {
              provinces[j] = provinces[k];
              stack.addFirst(j);
            }
          }
        }
      }
    }

    return numProvinces;
  }
}