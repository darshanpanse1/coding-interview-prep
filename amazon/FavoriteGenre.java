import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Given a map Map<String, List<String>> userSongs with user names as keys and a
 * list of all the songs that the user has listened to as values.
 * 
 * Also given a map Map<String, List<String>> songGenres, with song genre as
 * keys and a list of all the songs within that genre as values. The song can
 * only belong to only one genre.
 * 
 * The task is to return a map Map<String, List<String>>, where the key is a
 * user name and the value is a list of the user's favorite genre(s). Favorite
 * genre is the most listened to genre. A user can have more than one favorite
 * genre if he/she has listened to the same number of songs per each of the
 * genres.
 * 
 * Example 1:
 * 
 * Input: userSongs = { 
 *   "David": ["song1", "song2", "song3", "song4", "song8"],
 *   "Emma": ["song5", "song6", "song7"] 
 * }, 
 * songGenres = { 
 *   "Rock": ["song1", "song3"],
 *   "Dubstep": ["song7"],
 *   "Techno": ["song2", "song4"],
 *   "Pop": ["song5", "song6"],
 *   "Jazz": ["song8", "song9"] 
 * }
 * 
 * Output: { "David": ["Rock", "Techno"], "Emma": ["Pop"] }
 * 
 * Explanation: David has 2 Rock, 2 Techno and 1 Jazz song. So he has 2 favorite
 * genres. Emma has 2 Pop and 1 Dubstep song. Pop is Emma's favorite genre.
 * Example 2:
 * 
 * Input: userSongs = { "David": ["song1", "song2"], "Emma": ["song3", "song4"]
 * }, songGenres = {}
 * 
 * Output: { "David": [], "Emma": [] }
 */
public class FavoriteGenre {

  public Map<String, List<String>> findFavoriteGenres(Map<String, List<String>> userSongs,
      Map<String, List<String>> songGenres) {
    Map<String, List<String>> result = new HashMap<>();
    
    Map<String, String> invertedSongGenres = invertSongGenre(songGenres);
    
    for (Map.Entry<String, List<String>> e: userSongs.entrySet()) {
      if (songGenres.isEmpty()) {
        result.put(e.getKey(), new ArrayList<>());
      } else {
        List<String> songs = e.getValue();
        Map<String, Integer> freqMap = new HashMap<>();

        List<String> genres = new ArrayList<>();
        for (String song: songs) {
          genres.add(invertedSongGenres.get(song));   
        }

        for (String genre: genres) {
          if (freqMap.containsKey(genre)) {
            freqMap.put(genre, freqMap.get(genre) + 1);
          } else {
            freqMap.put(genre, 1);
          }
        }
        
        List<Map.Entry<String, Integer>> freqs = new ArrayList<>(freqMap.entrySet());
        Collections.sort(freqs, (Map.Entry<String, Integer> e1, Map.Entry<String, Integer> e2) -> {
          if (e1.getValue() > e2.getValue()) return -1;
          if (e1.getValue() == e2.getValue()) return 0;
          return 1;
        });

        List<String> favoriteGenres = new ArrayList<>();
        int maxFreq = freqs.get(0).getValue();
        for (Map.Entry<String, Integer> entry: freqs) {
          if (entry.getValue() == maxFreq) {
            favoriteGenres.add(entry.getKey());
          }
        }

        result.put(e.getKey(), favoriteGenres);
      }
    }

    return result;
  }

  private Map<String, String> invertSongGenre(Map<String, List<String>> songGenres) {
    Map<String, String> result = new HashMap<>();
    if (!songGenres.isEmpty()) {
      for (Map.Entry<String, List<String>> e: songGenres.entrySet()) {
        for (String song: e.getValue()) {
          result.put(song, e.getKey());
        }
      }
    }
    
    return result;
  }

  private static void prettyPrint(Map<String, List<String>> result) {
    for (Map.Entry<String, List<String>> e: result.entrySet()) {
      String user = e.getKey();
      String genres = e.getValue().stream()
      .map(n -> String.valueOf(n))
      .collect(Collectors.joining(",", "{", "}"));

      System.out.println(user + " : " + genres);
    }
  }

  public static void main(String[] args) {
    FavoriteGenre favoriteGenre = new FavoriteGenre();
    test1(favoriteGenre);
    System.out.println();
    test2(favoriteGenre);
  }

  private static void test1(FavoriteGenre favoriteGenre) {
    Map<String, List<String>> userSongs = new HashMap<>();
    userSongs.put("David", List.of("song1", "song2", "song3", "song4", "song8"));
    userSongs.put("Emma", List.of("song5", "song6", "song7"));

    Map<String, List<String>> songGenres = new HashMap<>();
    songGenres.put("Rock", List.of("song1", "song3"));
    songGenres.put("Dubstep", List.of("song7"));
    songGenres.put("Techno", List.of("song2", "song4"));
    songGenres.put("Pop", List.of("song5", "song6"));
    songGenres.put("Jazz", List.of("song8", "song9"));

    Map<String, List<String>> result = favoriteGenre.findFavoriteGenres(userSongs, songGenres);
    prettyPrint(result);
  }

  private static void test2(FavoriteGenre favoriteGenre) {
    Map<String, List<String>> userSongs = new HashMap<>();
    userSongs.put("David", List.of("song1", "song2", "song3", "song4", "song8"));
    userSongs.put("Emma", List.of("song5", "song6", "song7"));

    Map<String, List<String>> songGenres = new HashMap<>();
    Map<String, List<String>> result = favoriteGenre.findFavoriteGenres(userSongs, songGenres);
    prettyPrint(result);
  }
}
