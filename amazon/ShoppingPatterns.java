import java.util.*;

/**
 * A customer wants to buy a pair of jeans, a pair of shoes, a skirt, and a top
 * but has a limited budget in dollars. Given different pricing options for each
 * product, determine how many options our customer has to buy 1 of each
 * product. You cannot spend more money than the budgeted amount.
 * 
 * Example priceOfJeans = [2, 3] priceOfShoes = [4] priceOfSkirts = [2, 3]
 * priceOfTops = [1, 2] budgeted = 10
 * 
 * The customer must buy shoes for 4 dollars since there is only one option.
 * This leaves 6 dollars to spend on the other 3 items. Combinations of prices
 * paid for jeans, skirts, and tops respectively that add up to 6 dollars or
 * less are [2, 2, 2], [2, 2, 1], [3, 2, 1], [2, 3, 1]. There are 4 ways the
 * customer can purchase all 4 items.
 * 
 * Function Description
 * 
 * Complete the getNumberOfOptions function in the editor below. The function
 * must return an integer which represents the number of options present to buy
 * the four items.
 * 
 * getNumberOfOptions has 5 parameters: int[] priceOfJeans: An integer array,
 * which contains the prices of the pairs of jeans available. int[]
 * priceOfShoes: An integer array, which contains the prices of the pairs of
 * shoes available. int[] priceOfSkirts: An integer array, which contains the
 * prices of the skirts available. int[] priceOfTops: An integer array, which
 * contains the prices of the tops available. int dollars: the total number of
 * dollars available to shop with.
 * 
 * Constraints
 * 
 * 1 ≤ a, b, c, d ≤ 103 1 ≤ dollars ≤ 109 1 ≤ price of each item ≤ 109 Note: a,
 * b, c and d are the sizes of the four price arrays
 */

 public class ShoppingPatterns {
   public int getNumberOfOptions(int[] pairOfJeans, int[] pairOfShoes, int[] skirts, int[] tops, int budget) {
    Map<Integer, Integer> map = new HashMap<>();

    for (int skirtPrice: skirts) {
      for (int topPrice: tops) {
        int sum = skirtPrice + topPrice;
        map.put(sum, map.getOrDefault(sum, 0) + 1);    
      }
    }
    
    int count = 0;
    
    for (int jeansPrice: pairOfJeans) {
      for (int shoesPrice: pairOfShoes) {
        int remainingBudget = budget - (jeansPrice + shoesPrice);
        for (Map.Entry<Integer, Integer> e: map.entrySet()) {
          if (e.getKey() <= remainingBudget) {
            count += e.getValue();
          }
        }
      }
    }

    return count;
  }

  public static void main(String[] args) {
    ShoppingPatterns sp = new ShoppingPatterns();
    test1(sp);
    test2(sp);
    test3(sp);
  }

  private static void test1(ShoppingPatterns sp) {
    int[] pairOfJeans = {2};
    int[] pairOfShoes = {2, 2};
    int[] skirts = {2};
    int[] tops = {2};
    int budget = 9;

    System.out.println(sp.getNumberOfOptions(pairOfJeans, pairOfShoes, skirts, tops, budget));
  }

  private static void test2(ShoppingPatterns sp) {
    int[] pairOfJeans = {4, 7};
    int[] pairOfShoes = {6, 6};
    int[] skirts = {1, 3, 5};
    int[] tops = {5, 7, 12};
    int budget = 20;

    System.out.println(sp.getNumberOfOptions(pairOfJeans, pairOfShoes, skirts, tops, budget));
  }

  private static void test3(ShoppingPatterns sp) {
    int[] pairOfJeans = {2};
    int[] pairOfShoes = {3, 4};
    int[] skirts = {2, 5};
    int[] tops = {4, 6};
    int budget = 12;

    System.out.println(sp.getNumberOfOptions(pairOfJeans, pairOfShoes, skirts, tops, budget));
  }
}