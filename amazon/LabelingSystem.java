/**
 * Given a string, construct a new string by rearranging the original string and
 * deleting characters as needed. Return the alphabetically largest string that
 * can be constructed respecting a limit as to how many consecutive characters
 * can be the same.
 * 
 * Example: s='bacc' k=2
 * 
 * The largest string, alphabetically, is 'cccba' but it is not allowed because
 * it uses the character 'c' more than 2 times consecutively. Therefore, the
 * answer is 'ccbca'.
 * 
 * Function Description Complete the function getLargestString in the editor
 * below.
 * 
 * getLargestString has the following parameters: string s[n]: the original
 * string int k: the maximum number of identical consecutive characters the new
 * string can have
 * 
 * Returns: string: the alphabetically largest string that can be constructed
 * that has no more than k identical consecutive characters
 * 
 * Constraints
 * 
 * 1<= n <= 10^5 1<= k <= 10^3Ò The string s contains only lowercase English
 * letters. Input Format For Custom Testing Sample Case 0 Sample Input STDIN
 * Function
 * 
 * zzzazz --> string s = 'zzzazz' 2 --> k = 2
 * 
 * Sample Output zzazz
 * 
 * Explanation One 'z' must be removed so that no more than 2 consecutive
 * characters are the same.
 */

public class LabelingSystem {
  public String getLargestString(String s, int k) {
    int ALPHABET_SIZE = 26;
    // create a frequency array as in bucket sort.
    int[] buckets = new int[ALPHABET_SIZE];
    for (char c : s.toCharArray()) {
      buckets[c - 'a']++;
    }

    int r = ALPHABET_SIZE - 1, l = r - 1;
    StringBuilder result = new StringBuilder();

    while (r >= 0) {
      int count = 0;
      while (buckets[r] > 0 && count < k) {
        result.append((char) (r + 'a'));
        count++;
        buckets[r]--;
      }
      if (buckets[r] == 0 && r != 0) {
        r = l;
        l--;
      } else {
        while (l >= 0 && buckets[l] == 0) {
          l--;
        }
        if (l == -1) {
          break;
        }
        result.append((char) (l + 'a'));
        buckets[l]--;
      }
    }

    return result.toString();
  }

  public static void main(String[] args) {
    LabelingSystem ls = new LabelingSystem();
    test1(ls);
    test2(ls);
    test3(ls);
    test4(ls);
  }

  private static void test1(LabelingSystem ls) {
    int k = 2;
    String s = "baccc";
    System.out.println(ls.getLargestString(s, k));
  }

  private static void test2(LabelingSystem ls) {
    int k = 2;
    String s = "zzzazz";
    System.out.println(ls.getLargestString(s, k));
  }

  private static void test3(LabelingSystem ls) {
    int k = 2;
    String s = "zzzzz";
    System.out.println(ls.getLargestString(s, k));
  }

  private static void test4(LabelingSystem ls) {
    int k = 1;
    String s = "aaaaa";
    System.out.println(ls.getLargestString(s, k));
  }
}